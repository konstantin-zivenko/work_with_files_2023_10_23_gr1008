# try:
#     file = open("files/dog.txt")
# finally:
#     file.close()
#
#
# with open("fils/dog.txt") as file:
#     ...


with open("files/dog.txt", "r") as reader:
    line = reader.readline()
    while line != "":
        print(line, end="")
        line = reader.readline()


with open("files/dog.txt", "r") as reader:
    for line in reader.readlines():
        print(line, end="")


with open("files/dog.txt") as reader:
    for line in reader:
        print(line, end="")


with open("files/dog.txt", "r") as reader:
    lines = reader.readlines()


with open("files/dog.txt", "w") as writer:
    writer.writelines(reversed(lines))


with open("files/dog.txt", "w") as writer:
    for elem in reversed(lines):
        writer.write(elem)
